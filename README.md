##### Credit card fraud classification | Classification on imbalanced dataset

The aim of this project is to explore effectiveness of supervised machine learning algorithms in identifying fraud transactions using the anonimised credit card transactional data.

There are various issues when working with highly imbalanced datasets and in this notebook they will be addressed by using class weights in model parameters and creating a less imbalanced dataset. The results will be compared against default setting models and a standard dataset.

## Running this notebook:

Clone it > Download the dataset from the link and put it into the same folder > Run it

Dataset is from Kaggle - https://www.kaggle.com/mlg-ulb/creditcardfraud